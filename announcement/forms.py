from django import forms
from multiupload.fields import MultiImageField

from announcement.constants import MAX_PICTURE_IN_GALLERY
from announcement.models import Announcement


class AnnouncementForm(forms.ModelForm):
    status = forms.CharField(widget=forms.HiddenInput())
    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=100)
    content = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control'}),
        max_length=4000)
    place = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=100, required=True)
    tags = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=100, required=False)
    price = forms.IntegerField(required=True)
    internet_included = forms.BooleanField(required=False)
    disabled_utilities = forms.BooleanField(required=False)
    lift = forms.BooleanField(required=False)
    room_shared = forms.BooleanField(required=False)
    balcony = forms.BooleanField(required=False)
    image = forms.ImageField(required=False)
    gallery_images = MultiImageField(required=False, min_num=1, max_num=MAX_PICTURE_IN_GALLERY,
                                     max_file_size=1024 * 1024 * 5,
                                     help_text="Maximum {}".format(MAX_PICTURE_IN_GALLERY))

    class Meta:
        model = Announcement
        fields = [
            'title', 'content', 'place', 'tags', 'status', 'price', 'internet_included', 'disabled_utilities', 'lift',
            'room_shared', 'balcony', 'image'
        ]

    def __init__(self, *args, **kwargs):
        super(AnnouncementForm, self).__init__(*args, **kwargs)
