from django.contrib import admin

from announcement.models import Announcement, Utilities, GalleryImage, AnnouncementComment


class GalleryImageInline(admin.TabularInline):
    model = GalleryImage


class UtilitiesAdminInline(admin.TabularInline):
    model = Utilities


class AnnouncementCommentAdminInline(admin.TabularInline):
    model = AnnouncementComment


class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('title', 'create_user', 'create_date')
    inlines = (GalleryImageInline, UtilitiesAdminInline, AnnouncementCommentAdminInline, )
    search_fields = ('title', 'slug', 'content', 'create_user')


admin.site.register(Announcement, AnnouncementAdmin)
