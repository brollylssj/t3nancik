# coding: utf-8

from django.conf.urls import patterns, include, url

from announcement import views

urlpatterns = [
    url(r'^$', views.AnnouncementsView.as_view(), name='announcement'),
    url(r'^write/$', views.WriteView.as_view(), name='write'),
    url(r'^preview/$', views.PreviewView.as_view(), name='preview'),
    url(r'^drafts/$', views.DraftsView.as_view(), name='drafts'),
    url(r'^comment/$', views.CommentView.as_view(), name='comment'),
    url(r'^tag/(?P<tag_name>.+)/$', views.TagView.as_view(), name='tag'),
    url(r'^edit/(?P<id>\d+)/$', views.EditView.as_view(), name='edit_announcement'),
    url(r'^edit/(?P<id>\d+)/delete/(?P<image_id>\d+)/$', views.DeleteGalleryImageView.as_view(), name='delete_gallery_image'),
    url(r'^(?P<slug>[-\w]+)/$', views.AnnouncementView.as_view(), name='announcement'),
    # url(r'^write/gallery_upload/$', views.GalleryUploadView.as_view(), name='gallery_upload'),

]