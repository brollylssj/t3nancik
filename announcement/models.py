from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from datetime import datetime
from django.template.defaultfilters import slugify
import markdown
from announcement.constants import AMOUNT_OF_TAGS_DISPLAY


class Announcement(models.Model):
    DRAFT = 'D'
    PUBLISHED = 'P'
    STATUS = (
        (DRAFT, 'Draft'),
        (PUBLISHED, 'Published'),
    )

    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, null=True, blank=True)
    content = models.TextField(max_length=4000)
    status = models.CharField(max_length=1, choices=STATUS, default=DRAFT)
    create_user = models.ForeignKey(User)
    image = models.ImageField(upload_to='announcement_images/%Y/%m/%d', default=None, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(blank=True, null=True)
    price = models.IntegerField(default=0)
    place = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = _("Announcement")
        verbose_name_plural = _("Announcements")
        ordering = ("-create_date",)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Announcement, self).save(*args, **kwargs)
        else:
            self.update_date = datetime.now()
        if not self.slug:
            slug_str = "%s %s" % (self.pk, self.title.lower())
            self.slug = slugify(slug_str)
        super(Announcement, self).save(*args, **kwargs)

    def get_content_as_markdown(self):
        return markdown.markdown(self.content, safe_mode='escape')

    @staticmethod
    def get_published():
        announcements = Announcement.objects.filter(status=Announcement.PUBLISHED)
        return announcements

    def create_tags(self, tags):
        tags = tags.strip()
        tag_list = tags.split(' ')
        for tag in tag_list:
            if tag:
                Tag.objects.get_or_create(tag=tag.lower(), announcement=self)

    def get_tags(self):
        return Tag.objects.filter(announcement=self)

    def get_summary(self):
        if len(self.content) > 255:
            return u'{0}...'.format(self.content[:255])
        else:
            return self.content

    def get_summary_as_markdown(self):
        return markdown.markdown(self.get_summary(), safe_mode='escape')

    def get_comments(self):
        return AnnouncementComment.objects.filter(announcement=self)


class Utilities(models.Model):
    internet_included = models.NullBooleanField(default=False)
    disabled_utilities = models.NullBooleanField(default=False)
    lift = models.NullBooleanField(default=False)
    room_shared = models.NullBooleanField(default=False)
    balcony = models.NullBooleanField(default=False)
    announcement = models.OneToOneField(Announcement, blank=True, null=True)

    class Meta:
        verbose_name = _("Utilities")
        verbose_name_plural = _("Utilities")


class GalleryImage(models.Model):
    attachment = models.FileField(upload_to='gallery_attachments/%Y/%m/%d')
    announcement_gallery = models.ForeignKey(Announcement, default=None, blank=True, null=True)


class Tag(models.Model):
    tag = models.CharField(max_length=50)
    announcement = models.ForeignKey(Announcement)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
        unique_together = (('tag', 'announcement'),)
        index_together = [['tag', 'announcement'], ]

    def __unicode__(self):
        return self.tag

    @staticmethod
    def get_popular_tags():
        tags = Tag.objects.all()
        count = {}
        for tag in tags:
            if tag.announcement.status == Announcement.PUBLISHED:
                if tag.tag in count:
                    count[tag.tag] = + 1
                else:
                    count[tag.tag] = 1
        sorted_count = sorted(count.items(), key=lambda t: t[1], reverse=True)
        return sorted_count[:AMOUNT_OF_TAGS_DISPLAY]


class AnnouncementComment(models.Model):
    announcement = models.ForeignKey(Announcement)
    comment = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)

    class Meta:
        verbose_name = _("Announcement Comment")
        verbose_name_plural = _("Announcement Comments")
        ordering = ("date",)

    def __unicode__(self):
        return u'{} - {}'.format(self.user.username, self.announcement.title)