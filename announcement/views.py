import markdown
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import View

from announcement.constants import EMPTY_PREVIEW_MSG, MAX_PICTURE_IN_GALLERY
from announcement.forms import AnnouncementForm
from announcement.models import Announcement, Tag, AnnouncementComment, GalleryImage, Utilities
from tenancik.decorators import ajax_required


class AnnouncementViewMixin(TemplateView):
    template_name = 'announcement/announcements.html'

    def get_paginated_announcements(self, request, announcements):
        paginator = Paginator(announcements, 10)
        page = request.GET.get('page')
        try:
            announcements = paginator.page(page)
        except PageNotAnInteger:
            announcements = paginator.page(1)
        except EmptyPage:
            announcements = paginator.page(paginator.num_pages)
        popular_tags = Tag.get_popular_tags()
        return render(request, self.template_name, {
            'announcements': announcements,
            'popular_tags': popular_tags
        })


class AnnouncementsView(AnnouncementViewMixin):
    def get(self, request, *args, **kwargs):
        all_announcements = Announcement.get_published()
        return self.get_paginated_announcements(request, all_announcements)


class AnnouncementView(TemplateView):
    template_name = 'announcement/announcement.html'
    success_url = reverse_lazy('announcement')

    def get(self, request, slug):
        announcement = get_object_or_404(Announcement, slug=slug, status=Announcement.PUBLISHED)
        gallery_images = GalleryImage.objects.filter(announcement_gallery=announcement)
        utilities = get_object_or_404(Utilities, announcement=announcement)
        return render(request, self.template_name,
                      {'announcement': announcement, 'gallery_images': gallery_images, 'utilities': utilities})

    def post(self, request, slug):
        announcement = get_object_or_404(Announcement, slug=slug, status=Announcement.PUBLISHED)
        Utilities.objects.filter(announcement=announcement).delete()
        GalleryImage.objects.filter(announcement_gallery=announcement).delete()
        announcement.delete()
        return redirect(self.success_url)


class TagView(AnnouncementViewMixin):
    def get(self, request, tag_name):
        tags = Tag.objects.filter(tag=tag_name)
        announcements = []
        for tag in tags:
            if tag.announcement.status == Announcement.PUBLISHED:
                announcements.append(tag.announcement)
        return self.get_paginated_announcements(request, announcements)


class AnnouncementMixin(object):
    class_form = AnnouncementForm

    def add_gallery_images_to_announcement(self, announcement, form):
        gallery_images = form.cleaned_data.get('gallery_images')
        for gallery_image in gallery_images:
            gallery_objects = GalleryImage.objects.filter(announcement_gallery=announcement)
            if len(gallery_objects) < MAX_PICTURE_IN_GALLERY:
                GalleryImage.objects.create(attachment=gallery_image, announcement_gallery=announcement)

    def add_utilities_to_announcement(self, announcement, form):
        internet_included = form.cleaned_data.get('internet_included')
        disabled_utilities = form.cleaned_data.get('disabled_utilities')
        lift = form.cleaned_data.get('lift')
        room_shared = form.cleaned_data.get('room_shared')
        balcony = form.cleaned_data.get('balcony')
        Utilities.objects.create(internet_included=internet_included, disabled_utilities=disabled_utilities, lift=lift,
                                 room_shared=room_shared, balcony=balcony, announcement=announcement)

    def edit_utilities_to_announcement(self, announcement, form):
        utilities = get_object_or_404(Utilities, announcement=announcement)
        utilities.internet_included = form.cleaned_data.get('internet_included')
        utilities.disabled_utilities = form.cleaned_data.get('disabled_utilities')
        utilities.lift = form.cleaned_data.get('lift')
        utilities.room_shared = form.cleaned_data.get('room_shared')
        utilities.balcony = form.cleaned_data.get('balcony')
        utilities.save()


class WriteView(TemplateView, AnnouncementMixin):
    template_name = 'announcement/write.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        form = self.class_form()
        return render(request, self.template_name, {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.class_form(request.POST, request.FILES)
        if form.is_valid():
            title = form.cleaned_data.get('title')
            price = form.cleaned_data.get('price')
            place = form.cleaned_data.get('place')
            content = form.cleaned_data.get('content')
            image = form.cleaned_data.get('image')
            announcement = Announcement(create_user=request.user, title=title, content=content, image=image,
                                        price=price, place=place)
            status = form.cleaned_data.get('status')
            if status in [Announcement.PUBLISHED, Announcement.DRAFT]:
                announcement.status = form.cleaned_data.get('status')
            announcement.save()
            self.add_gallery_images_to_announcement(announcement, form)
            self.add_utilities_to_announcement(announcement, form)
            tags = form.cleaned_data.get('tags')
            announcement.create_tags(tags)
            return redirect('/announcement/')
        return render(request, self.template_name, {'form': form})


class DraftsView(TemplateView):
    template_name = 'announcement/drafts.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        drafts = Announcement.objects.filter(create_user=request.user, status=Announcement.DRAFT)
        return render(request, self.template_name, {'drafts': drafts})


class EditView(TemplateView, AnnouncementMixin):
    template_name = 'announcement/edit.html'

    @method_decorator(login_required)
    def get(self, request, id):
        tags = ''
        announcement = get_object_or_404(Announcement, pk=id)
        for tag in announcement.get_tags():
            tags = u'{} {}'.format(tags, tag.tag)
        tags = tags.strip()
        if announcement.create_user.id != request.user.id:
            return redirect('home')
        utilities = get_object_or_404(Utilities, announcement=announcement)
        gallery_images = list(GalleryImage.objects.filter(announcement_gallery=announcement))
        initial = {
            'tags': tags,
            'internet_included': utilities.internet_included,
            'disabled_utilities': utilities.disabled_utilities,
            'lift': utilities.lift,
            'room_shared': utilities.room_shared,
            'balcony': utilities.balcony,
        }
        form = self.class_form(instance=announcement, initial=initial)
        return render(request, self.template_name, {'form': form, 'gallery_images': gallery_images})

    @method_decorator(login_required)
    def post(self, request, id):
        announcement = get_object_or_404(Announcement, pk=id)
        form = self.class_form(request.POST, request.FILES, instance=announcement)
        if form.is_valid():
            self.add_gallery_images_to_announcement(announcement, form)
            self.edit_utilities_to_announcement(announcement, form)
            form.save()
            return redirect('/announcement/')
        return render(request, self.template_name, {'form': form})


class DeleteGalleryImageView(View):
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def post(self, *args, **kwargs):
        image_id = kwargs.get('image_id', None)
        id = kwargs.get('id', None)
        GalleryImage.objects.filter(id=image_id).delete()
        return redirect('edit_announcement', id)


class PreviewView(TemplateView):
    html = EMPTY_PREVIEW_MSG

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        content = request.POST.get('content')
        if len(content.strip()) > 0:
            self.html = markdown.markdown(content, safe_mode='escape')
        return HttpResponse(self.html)


class CommentView(TemplateView):
    template_name = 'announcement/partial_announcement_comment.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        announcement_id = request.POST.get('announcement')
        announcement = Announcement.objects.get(pk=announcement_id)
        comment = request.POST.get('comment')
        comment = comment.strip()
        if len(comment) > 0:
            announcement_comment = AnnouncementComment(user=request.user, announcement=announcement, comment=comment)
            announcement_comment.save()
            request.user.profile.answer_on_announcement(announcement=announcement)
        html = u''
        for comment in announcement.get_comments():
            html = u'{}{}'.format(html, render_to_string(self.template_name, {'comment': comment}))
        return HttpResponse(html)
