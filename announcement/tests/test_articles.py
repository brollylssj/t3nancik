from django.test import TestCase, Client, RequestFactory
from django.core.urlresolvers import reverse
from announcement.models import Announcement
from django.contrib.auth.models import User


class AnnouncementTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

    def test_validate_article_edition(self):
        user2 = User.objects.create_user(username="teste12345", email="reallynice2@gmail.com", password="supersecret123")

        announcement = Announcement()
        announcement.title = "nicetitle"
        announcement.content = "nicecontent"
        announcement.create_user = user2
        announcement.create_user.id = user2.id
        announcement.save()
        self.client.login(username="teste1234", password="supersecret123")
        response = self.client.get(reverse('edit_article', kwargs={'id': '1'}))
        self.assertEqual(response.status_code, 302)
        response = self.client.post(reverse('edit_article', kwargs={'id': '1'}))
        self.assertEqual(response.status_code, 302)
        self.client.login(username="teste12345", password="supersecret123")
        response = self.client.get(reverse('edit_article', kwargs={'id': '1'}), user=user2)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse('edit_article', kwargs={'id': '1'}))
        self.assertEqual(response.status_code, 200)
