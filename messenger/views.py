import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import View

from messenger.exceptions import MessengerException
from messenger.messenger_service import MessengerService
from messenger.models import Message
from tenancik.decorators import ajax_required


class InboxView(TemplateView):
    template_name = 'messenger/inbox.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        conversations = Message.get_conversations(user=request.user)
        active_conversation = None
        messages = None
        if conversations:
            conversation = conversations[0]
            active_conversation = conversation['user'].username
            messages = Message.objects.filter(user=request.user, conversation=conversation['user'])
            messages.update(is_read=True)
            for conversation in conversations:
                if conversation['user'].username == active_conversation:
                    conversation['unread'] = 0
        context = {
            'messages': messages,
            'conversations': conversations,
            'active': active_conversation
        }
        return render(request, self.template_name, context)


class MessagesView(TemplateView):
    template_name = 'messenger/inbox.html'

    @method_decorator(login_required)
    def get(self, request, username):
        conversations = Message.get_conversations(user=request.user)
        active_conversation = username
        messages = Message.objects.filter(user=request.user, conversation__username=username)
        messages.update(is_read=True)
        for conversation in conversations:
            if conversation['user'].username == username:
                conversation['unread'] = 0
        context = {
            'messages': messages,
            'conversations': conversations,
            'active': active_conversation
        }
        return render(request, self.template_name, context)


class NewMessageView(TemplateView):
    template_name = 'messenger/new.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        conversations = Message.get_conversations(user=request.user)
        return render(request, self.template_name, {'conversations': conversations})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        messenger = MessengerService(request)
        try:
            to_user_username = messenger.create_new_conversation()
        except MessengerException:
            return redirect('/messages/new/')
        return redirect(u'/messages/{}/'.format(to_user_username))


class SendMessageView(TemplateView):
    template_name = 'messenger/includes/partial_message.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        messenger = MessengerService(request)
        try:
            msg = messenger.send_message()
        except MessengerException:
            return HttpResponse()
        return render(request, self.template_name, {'message': msg})


class UserSearchMessageView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        users = User.objects.filter(is_active=True)
        dump = []
        template = u'{} ({})'
        for user in users:
            if user.profile.get_screen_name() != user.username:
                dump.append(template.format(user.profile.get_screen_name(), user.username))
            else:
                dump.append(user.username)
        data = json.dumps(dump)
        return HttpResponse(data, content_type='application/json')


class CheckMessageView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        count = Message.objects.filter(user=request.user, is_read=False).count()
        return HttpResponse(count)
