

class MessengerException(Exception):
    pass


class UserToMessageException(MessengerException):
    pass


class EmptyMessageException(MessengerException):
    pass