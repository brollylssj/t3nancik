# coding: utf-8

from django.conf.urls import url
from messenger import views
from messenger.views import MessagesView

urlpatterns = [
    url(r'^$', views.InboxView.as_view(), name='inbox'),
    url(r'^new/$', views.NewMessageView.as_view(), name='new_message'),
    url(r'^send/$', views.SendMessageView.as_view(), name='send_message'),
    url(r'^users/$', views.UserSearchMessageView.as_view(), name='users_message'),
    url(r'^check/$', views.CheckMessageView.as_view(), name='check_message'),
    url(r'^(?P<username>[^/]+)/$', MessagesView.as_view(), name='messages'),
]
