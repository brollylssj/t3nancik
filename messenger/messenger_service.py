from django.contrib import messages
from django.contrib.auth.models import User

import logging

from messenger.exceptions import UserToMessageException, EmptyMessageException
from messenger.models import Message

logger = logging.getLogger(__name__)


class MessengerService(object):
    def __init__(self, request):
        self.request = request
        self.from_user = request.user
        self.to_user_username = request.POST.get('to')
        self.message = request.POST.get('message')

    def create_new_conversation(self):
        try:
            to_user_username = self._get_to_user_username(self.to_user_username)
            to_user = User.objects.get(username=to_user_username)
        except Exception as e:
            logger.error('Error when try to catch to_user: %s', e.message)
            messages.error(self.request, 'User does not exist!')
            raise UserToMessageException
        self.send_valid_message(to_user)
        return to_user_username

    def send_message(self):
        to_user = User.objects.get(username=self.to_user_username)
        return self.send_valid_message(to_user)

    def send_valid_message(self, to_user):
        if len(self.message.strip()) == 0:
            raise EmptyMessageException
        return self.send_message_to_user(to_user)

    def send_message_to_user(self, to_user):
        if self.from_user != to_user:
            return Message.send_message(self.from_user, to_user, self.message)

    def _get_to_user_username(self, to_user_username):
        if '(' in to_user_username:
            return to_user_username[to_user_username.rfind('(')+1:len(to_user_username)-1]
        return to_user_username