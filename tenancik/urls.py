# coding: utf-8

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from solid_i18n.urls import solid_i18n_patterns

from activities.views import NotificationsView, LastNotificationsView, CheckNotificationView
from authentication.views import SignupView
from core.views import (
    HomeView, NetworkView, ProfileView, SettingsView, PictureChangeView, ChangePasswordView,
    SavePictureView, UploadPictureView
)
from search import views as search_views

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n', namespace='i18n')),
]

urlpatterns += solid_i18n_patterns(
    url(r'^$', HomeView.as_view(), name='home'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/rosetta/', include('rosetta.urls')),
    url(r'^login', auth_views.login, {'template_name': 'core/cover.html'}, name='login'),
    url(r'^logout', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^signup/$', SignupView.as_view(), name='signup'),
    url(r'^settings/$', SettingsView.as_view(), name='settings'),
    url(r'^settings/picture/$', PictureChangeView.as_view(), name='picture'),
    url(r'^settings/upload_picture/$', UploadPictureView.as_view(), name='upload_picture'),
    url(r'^settings/save_uploaded_picture/$', SavePictureView.as_view(), name='save_uploaded_picture'),
    url(r'^settings/password/$', ChangePasswordView.as_view(), name='password'),
    url(r'^network/$', NetworkView.as_view(), name='network'),
    url(r'^feeds/', include('feeds.urls')),
    url(r'^questions/', include('questions.urls')),
    url(r'^announcement/', include('announcement.urls')),
    url(r'^messages/', include('messenger.urls')),
    url(r'^notifications/$', NotificationsView.as_view(), name='notifications'),
    url(r'^notifications/last/$', LastNotificationsView.as_view(), name='last_notifications'),
    url(r'^notifications/check/$', CheckNotificationView.as_view(), name='check_notifications'),
    url(r'^search/$', search_views.SearchView.as_view(), name='search'),
    url(r'^(?P<username>[^/]+)/$', ProfileView.as_view(), name='profile'),
    url(r'social-auth/', include('social.apps.django_app.urls', namespace='social')),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
