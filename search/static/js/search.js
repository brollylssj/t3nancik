$(function () {
  $(".feed-results li").click(function () {
    var feed = $(this).attr("feed-id");
    location.href = "/feeds/" + feed + "/";
  });

  $(".announcement-results li").click(function () {
    var announcement = $(this).attr("announcement-slug");
    location.href = "/announcement/" + announcement + "/";
  });

  $(".questions-results li").click(function () {
    var question = $(this).attr("question-id");
    location.href = "/questions/" + question + "/";
  });

});