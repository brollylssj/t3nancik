from django.contrib.auth.models import User
from django.db.models import Q

from announcement.models import Announcement
from feeds.models import Feed
from questions.models import Question
from search.exceptions import EmptySearchQueryException


class SearchEngine(object):
    def search(self, querystring):
        if len(querystring) == 0:
            raise EmptySearchQueryException
        search_quantity = {}
        search_result = {
            'feed': self._get_feeds_by_search_query(querystring),
            'announcement': self._get_anouncements_by_search_query(querystring),
            'questions': self._get_questions_by_search_query(querystring),
            'users': self._get_users_by_search_query(querystring)
        }
        for key, value in search_result.iteritems():
            search_quantity[key] = value.count()
        return search_result, search_quantity

    def _get_users_by_search_query(self, querystring):
        return User.objects.filter(
            Q(username__icontains=querystring) | Q(first_name__icontains=querystring) | Q(
                last_name__icontains=querystring))

    def _get_questions_by_search_query(self, querystring):
        return Question.objects.filter(
            Q(title__icontains=querystring) | Q(description__icontains=querystring))

    def _get_anouncements_by_search_query(self, querystring):
        return Announcement.objects.filter(
            Q(title__icontains=querystring) | Q(content__icontains=querystring) | Q(price__icontains=querystring))

    def _get_feeds_by_search_query(self, querystring):
        return Feed.objects.filter(post__icontains=querystring, parent=None)
