from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from search.constants import BASIC_SEARCH_TYPE
from search.exceptions import EmptySearchQueryException
from search.search_engine import SearchEngine


class SearchView(TemplateView):
    template_name = 'search/search.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        if 'q' in request.GET:
            search_type = request.GET.get('type')
            if search_type not in BASIC_SEARCH_TYPE:
                search_type = 'announcement'
            querystring = self.request.GET.get('q').strip()
            search_engine = SearchEngine()
            try:
                search_result, search_quantity = search_engine.search(querystring)
            except EmptySearchQueryException:
                return redirect('/search/')
            return render(request, 'search/results.html', {
                'hide_search': True,
                'querystring': querystring,
                'active': search_type,
                'count': search_quantity,
                'results': search_result[search_type],
            })
        else:
            return render(request, self.template_name, {'hide_search': True})

