# coding: utf-8

from django.conf.urls import url

from feeds.views import (
    FeedsView, FeedView, LoadFeedsView, LoadNewFeedsView, CheckFeedsView, CreateFeedView,
    LikeFeedView, CommentFeedView, UpdateFeedView, TrackFeedCommentsView, RemoveFeedView
)
urlpatterns = [
    url(r'^$', FeedsView.as_view(), name='feeds'),
    url(r'^post/$', CreateFeedView.as_view(), name='post'),
    url(r'^like/$', LikeFeedView.as_view(), name='like'),
    url(r'^comment/$', CommentFeedView.as_view(), name='comment'),
    url(r'^load/$', LoadFeedsView.as_view(), name='load'),
    url(r'^check/$', CheckFeedsView.as_view(), name='check'),
    url(r'^load_new/$', LoadNewFeedsView.as_view(), name='load_new'),
    url(r'^update/$', UpdateFeedView.as_view(), name='update'),
    url(r'^track_comments/$', TrackFeedCommentsView.as_view(), name='track_comments'),
    url(r'^remove/$', RemoveFeedView.as_view(), name='remove_feed'),
    url(r'^(\d+)/$', FeedView.as_view(), name='feed'),
]
