import json

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseBadRequest,\
                        HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.template.context_processors import csrf
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import View

from activities.models import Activity
from feeds.constants import AMOUNT_FEEDS_ON_PAGE, MAX_LENGTH_OF_FEED
from feeds.models import Feed
from tenancik.decorators import ajax_required


class FeedsView(TemplateView):
    template_name = 'feeds/feeds.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        all_feeds = Feed.get_feeds()
        paginator = Paginator(all_feeds, AMOUNT_FEEDS_ON_PAGE)
        feeds = paginator.page(1)
        from_feed = -1
        if feeds:
            from_feed = feeds[0].id
        context = {
            'feeds': feeds,
            'from_feed': from_feed,
            'page': 1,
        }
        return render(request, self.template_name, context)


class FeedView(TemplateView):
    template_name = 'feeds/feed.html'

    @method_decorator(login_required)
    def get(self, request, pk):
        feed = get_object_or_404(Feed, pk=pk)
        return render(request, self.template_name, {'feed': feed})


class LoadFeedsView(TemplateView):
    template_name = 'feeds/partial_feed.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        from_feed = request.GET.get('from_feed')
        page = request.GET.get('page')
        feed_source = request.GET.get('feed_source')
        all_feeds = Feed.get_feeds(from_feed)
        if feed_source != 'all':
            all_feeds = all_feeds.filter(user__id=feed_source)
        paginator = Paginator(all_feeds, AMOUNT_FEEDS_ON_PAGE)
        try:
            feeds = paginator.page(page)
        except PageNotAnInteger:
            return HttpResponseBadRequest()
        except EmptyPage:
            feeds = []
        html = u''
        csrf_token = unicode(csrf(request)['csrf_token'])
        context = {
            'user': request.user,
            'csrf_token': csrf_token
        }
        for feed in feeds:
            context.update({'feed': feed})
            html = u'{}{}'.format(html, render_to_string(self.template_name, context))
        return HttpResponse(html)


class BaseFeedActionView(TemplateView):
    template_name = 'feeds/partial_feed.html'

    def get_feed_to_html(self, last_feed, user, csrf_token, feed_source='all'):
        feeds = Feed.get_feeds_after(last_feed)
        if feed_source != 'all':
            feeds = feeds.filter(user__id=feed_source)
        context = {
            'user': user,
            'csrf_token': csrf_token
        }
        html = u''
        for feed in feeds:
            context.update({'feed': feed})
            html = u'{}{}'.format(html, render_to_string(self.template_name, context))
        return html


class LoadNewFeedsView(BaseFeedActionView):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        last_feed = request.GET.get('last_feed')
        user = request.user
        csrf_token = unicode(csrf(request)['csrf_token'])
        html = self.get_feed_to_html(last_feed, user, csrf_token)
        return HttpResponse(html)


class CheckFeedsView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        last_feed = request.GET.get('last_feed')
        feed_source = request.GET.get('feed_source')
        feeds = Feed.get_feeds_after(last_feed)
        if feed_source != 'all':
            feeds = feeds.filter(user__id=feed_source)
        count = feeds.count()
        return HttpResponse(count)


class CreateFeedView(BaseFeedActionView):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        last_feed = request.POST.get('last_feed')
        user = request.user
        csrf_token = unicode(csrf(request)['csrf_token'])
        feed = Feed()
        feed.user = user
        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            feed.post = post[:MAX_LENGTH_OF_FEED]
            feed.save()
        html = self.get_feed_to_html(last_feed, user, csrf_token)
        return HttpResponse(html)


class LikeFeedView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        feed_id = request.POST['feed']
        feed = Feed.objects.get(pk=feed_id)
        user = request.user
        like = Activity.objects.filter(activity_type=Activity.LIKE, feed=feed_id, user=user)
        if like:
            user.profile.unotify_liked(feed)
            like.delete()
        else:
            like = Activity(activity_type=Activity.LIKE, feed=feed_id, user=user)
            like.save()
            user.profile.notify_liked(feed)
        return HttpResponse(feed.calculate_likes())


class CommentFeedView(TemplateView):
    template_name = 'feeds/partial_feed_comments.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        feed_id = request.POST['feed']
        feed = Feed.objects.get(pk=feed_id)
        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            post = post[:MAX_LENGTH_OF_FEED]
            user = request.user
            feed.comment(user=user, post=post)
            user.profile.notify_commented(feed)
            user.profile.notify_also_commented(feed)
        return render(request, self.template_name, {'feed': feed})

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
            feed_id = request.GET.get('feed')
            feed = Feed.objects.get(pk=feed_id)
            return render(request, self.template_name, {'feed': feed})


class UpdateFeedView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        first_feed = request.GET.get('first_feed')
        last_feed = request.GET.get('last_feed')
        feed_source = request.GET.get('feed_source')
        feeds = Feed.get_feeds().filter(id__range=(last_feed, first_feed))
        if feed_source != 'all':
            feeds = feeds.filter(user__id=feed_source)
        dump = {}
        for feed in feeds:
            dump[feed.pk] = {'likes': feed.likes, 'comments': feed.comments}
        data = json.dumps(dump)
        return HttpResponse(data, content_type='application/json')


class TrackFeedCommentsView(TemplateView):
    template_name = 'feeds/partial_feed_comments.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        feed_id = request.GET.get('feed')
        feed = Feed.objects.get(pk=feed_id)
        return render(request, self.template_name, {'feed': feed})


class RemoveFeedView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        try:
            feed_id = request.POST.get('feed')
            feed = Feed.objects.get(pk=feed_id)
            if feed.user == request.user:
                likes = feed.get_likes()
                parent = feed.parent
                for like in likes:
                    like.delete()
                feed.delete()
                if parent:
                    parent.calculate_comments()
                return HttpResponse()
            else:
                return HttpResponseForbidden()
        except Exception as e:
            return HttpResponseBadRequest()
