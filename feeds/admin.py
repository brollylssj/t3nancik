from django.contrib import admin

from feeds.models import Feed


class FeedAdmin(admin.ModelAdmin):
    list_display = ('post', 'likes', 'comments', 'date')
    search_fields = ('post', 'user')


admin.site.register(Feed, FeedAdmin)
