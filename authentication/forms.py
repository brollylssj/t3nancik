from django import forms
from django.contrib.auth.models import User

from authentication.validators import AuthenticationValidator


class SignUpForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=True,
    )
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Confirm your password",
        required=True)
    email = forms.CharField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}),
        required=True,
        max_length=75)

    class Meta:
        model = User
        exclude = ['last_login', 'date_joined']
        fields = ['username', 'email', 'password', 'confirm_password', ]

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        auth_validator = AuthenticationValidator()
        self.fields['username'].validators.append(auth_validator.validate_username_bad_characters)
        self.fields['username'].validators.append(auth_validator.validate_unique_username)
        self.fields['email'].validators.append(auth_validator.validate_unique_email)
        self.fields['email'].validators.append(auth_validator.validate_signup_domain)

    def clean(self):
        super(SignUpForm, self).clean()
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password and password != confirm_password:
            self._errors['password'] = self.error_class(['Passwords don\'t match'])
        if len(password) < 6:
            self._errors['password'] = self.error_class(['Password should have almost 6 characters'])
        return self.cleaned_data
