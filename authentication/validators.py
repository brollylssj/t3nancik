from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from authentication.constants import USERNAME_INVALID_CHARACTERS
from tenancik.settings import ALLOWED_SIGNUP_DOMAINS


class AuthenticationValidator(object):

    def validate_signup_domain(self, value):
        if '*' not in ALLOWED_SIGNUP_DOMAINS:
            try:
                domain = value[value.index("@"):]
                if domain not in ALLOWED_SIGNUP_DOMAINS:
                    raise ValidationError(u'Invalid domain. Allowed domains on this network: {}'.format(','.join(ALLOWED_SIGNUP_DOMAINS)))

            except Exception, e:
                raise ValidationError(u'Invalid domain. Allowed domains on this network: {}'.format(','.join(ALLOWED_SIGNUP_DOMAINS)))


    def validate_username_bad_characters(self, value):
        if value in USERNAME_INVALID_CHARACTERS:
            raise ValidationError('Enter a valid username.')


    def validate_unique_email(self, value):
        if User.objects.filter(email__iexact=value).exists():
            raise ValidationError('User with this Email already exists.')


    def validate_unique_username(self, value):
        if User.objects.filter(username__iexact=value).exists():
            raise ValidationError('User with this Username already exists.')