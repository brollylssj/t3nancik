from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.generic import View

from authentication.forms import SignUpForm
from feeds.models import Feed


class SignupView(TemplateView):
    template_name = 'authentication/signup.html'
    class_form = SignUpForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'form': self.class_form()})

    def post(self, request, *args, **kwargs):
        form = self.class_form(request.POST)
        if not form.is_valid():
            return render(request, self.template_name, {'form': form})
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        User.objects.create_user(username=username, password=password, email=email)
        user = authenticate(username=username, password=password)
        login(request, user)
        welcome_post = u'{} has joined the t3nancik.'.format(user.username, user.username)
        feed = Feed(user=user, post=welcome_post)
        feed.save()
        return redirect('/')
