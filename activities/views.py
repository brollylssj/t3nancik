from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from activities.constants import NUMBER_OF_DISPLAY_NOTIFICATION
from activities.models import Notification
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from tenancik.decorators import ajax_required


class NotificationsView(TemplateView):
    template_name = 'activities/notifications.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        notifications = Notification.objects.filter(to_user=user)
        unread = Notification.objects.filter(to_user=user, is_read=False)
        for notification in unread:
            notification.is_read = True
            notification.save()
        return render(request, self.template_name, {'notifications': notifications})


class LastNotificationsView(TemplateView):
    template_name = 'activities/last_notifications.html'

    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        notifications = Notification.objects.filter(to_user=user, is_read=False)[:NUMBER_OF_DISPLAY_NOTIFICATION]
        for notification in notifications:
            notification.is_read = True
            notification.save()
        return render(request, self.template_name, {'notifications': notifications})


class CheckNotificationView(TemplateView):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        notifications = Notification.objects.filter(to_user=user, is_read=False)[:NUMBER_OF_DISPLAY_NOTIFICATION]
        return HttpResponse(len(notifications))
