NUMBER_OF_DISPLAY_NOTIFICATION = 5

LIKED_TEMPLATE = u'<a href="/{0}/">{1}</a> liked your post: <a href="/feeds/{2}/">{3}</a>'
COMMENTED_TEMPLATE = u'<a href="/{0}/">{1}</a> commented on your post: <a href="/feeds/{2}/">{3}</a>'
FAVORITE_TEMPLATE = u'<a href="/{0}/">{1}</a> favorited your question: <a href="/questions/{2}/">{3}</a>'
ANSWERED_TEMPLATE = u'<a href="/{0}/">{1}</a> answered your question: <a href="/questions/{2}/">{3}</a>'
ACCEPTED_ANSWER_TEMPLATE = u'<a href="/{0}/">{1}</a> accepted your answer: <a href="/questions/{2}/">{3}</a>'
EDITED_ANNOUNCEMENT_TEMPLATE = u'<a href="/{0}/">{1}</a> edited your announcement: <a href="/announcement/{2}/">{3}</a>'
ANSWER_ANNOUNCEMENT_TEMPLATE = u'<a href="/{0}/">{1}</a> answer on your announcement: <a href="/announcement/{2}/">{3}</a>'
ALSO_COMMENTED_TEMPLATE = u'<a href="/{0}/">{1}</a> also commentend on the post: <a href="/feeds/{2}/">{3}</a>'
