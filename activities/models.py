from django.contrib.auth.models import User
from django.db import models
from django.utils.html import escape

from activities.constants import (
    LIKED_TEMPLATE, COMMENTED_TEMPLATE, FAVORITE_TEMPLATE, ANSWERED_TEMPLATE,
    ACCEPTED_ANSWER_TEMPLATE, EDITED_ANNOUNCEMENT_TEMPLATE, ANSWER_ANNOUNCEMENT_TEMPLATE, ALSO_COMMENTED_TEMPLATE
)
from announcement.models import Announcement


class Activity(models.Model):
    FAVORITE = 'F'
    LIKE = 'L'
    UP_VOTE = 'U'
    DOWN_VOTE = 'D'
    COMMENT = 'C'
    ACTIVITY_TYPES = (
        (FAVORITE, 'Favorite'),
        (LIKE, 'Like'),
        (UP_VOTE, 'Up Vote'),
        (DOWN_VOTE, 'Down Vote'),
        (COMMENT, 'Comment'),
    )

    user = models.ForeignKey(User)
    activity_type = models.CharField(max_length=1, choices=ACTIVITY_TYPES)
    date = models.DateTimeField(auto_now_add=True)
    feed = models.IntegerField(null=True, blank=True)
    question = models.IntegerField(null=True, blank=True)
    answer = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'

    def __unicode__(self):
        return self.activity_type


# def save(self, *args, **kwargs):
#        super(Activity, self).save(*args, **kwargs)
#        if self.activity_type == Activity.FAVORITE:
#            Question = models.get_model('questions', 'Question')
#            question = Question.objects.get(pk=self.question)
#            user = question.user
#            user.profile.reputation = user.profile.reputation + 5
#            user.save()


class Notification(models.Model):
    LIKED = 'LIKED'
    COMMENTED = 'COMMENTED'
    FAVORITED = 'FAVORITED'
    ANSWERED = 'ANSWERED'
    ACCEPTED_ANSWER = 'ACCEPTED_ANSWER'
    EDITED_ANNOUNCEMENT = 'EDITED_ANNOUNCEMENT'
    ALSO_COMMENTED = 'ALSO_COMMENTED'
    ANSWER_ON_ANNOUNCEMENT = 'ANSWER_ON_ANNOUNCEMENT'
    NOTIFICATION_TYPES = (
        (LIKED, 'Liked'),
        (COMMENTED, 'Commented'),
        (FAVORITED, 'Favorited'),
        (ANSWERED, 'Answered'),
        (ACCEPTED_ANSWER, 'Accepted Answer'),
        (EDITED_ANNOUNCEMENT, 'Edited Announcement'),
        (ALSO_COMMENTED, 'Also Commented'),
        (ANSWER_ON_ANNOUNCEMENT, 'Answer on Announcement'),
    )

    from_user = models.ForeignKey(User, related_name='+')
    to_user = models.ForeignKey(User, related_name='+')
    date = models.DateTimeField(auto_now_add=True)
    feed = models.ForeignKey('feeds.Feed', null=True, blank=True)
    question = models.ForeignKey('questions.Question', null=True, blank=True)
    answer = models.ForeignKey('questions.Answer', null=True, blank=True)
    announcement = models.ForeignKey(Announcement, null=True, blank=True)
    notification_type = models.CharField(max_length=1, choices=NOTIFICATION_TYPES)
    is_read = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'
        ordering = ('-date',)

    NOTIFICATION_ACTION_QUESTION = {
        FAVORITED: FAVORITE_TEMPLATE,
        ANSWERED: ANSWERED_TEMPLATE,
        ACCEPTED_ANSWER: ACCEPTED_ANSWER_TEMPLATE,
    }

    NOTIFICATION_ACTION_FEED = {
        LIKED: LIKED_TEMPLATE,
        COMMENTED: COMMENTED_TEMPLATE,
        ALSO_COMMENTED: ALSO_COMMENTED_TEMPLATE,
    }

    NOTIFICATION_ACTION_ANNOUNCEMENT = {
        EDITED_ANNOUNCEMENT: EDITED_ANNOUNCEMENT_TEMPLATE,
        ANSWER_ON_ANNOUNCEMENT: ANSWER_ANNOUNCEMENT_TEMPLATE,
    }

    @property
    def get_message(self):
        feed = self.NOTIFICATION_ACTION_FEED.get(str(self.notification_type), None)
        question = self.NOTIFICATION_ACTION_QUESTION.get(str(self.notification_type), None)
        announcement = self.NOTIFICATION_ACTION_ANNOUNCEMENT.get(str(self.notification_type), None)
        return {'feed': feed, 'question': question, 'announcement': announcement}

    def _get_announcement_message(self, module):
        return self.get_message.get(module).format(
            escape(self.from_user.username),
            escape(self.from_user.profile.get_screen_name()),
            self.announcement.slug,
            escape(self.get_summary(self.announcement.title))
        )

    def _get_question_message(self, module):
        return self.get_message.get(module).format(
            escape(self.from_user.username),
            escape(self.from_user.profile.get_screen_name()),
            self.question.pk,
            escape(self.get_summary(self.question.title))
        )

    def _get_feed_message(self, module):
        return self.get_message.get(module).format(
            escape(self.from_user.username),
            escape(self.from_user.profile.get_screen_name()),
            self.feed.pk,
            escape(self.get_summary(self.feed.post)))

    def __unicode__(self):
        for module, template in self.get_message.iteritems():
            if module == 'feed' and template:
                return self._get_feed_message(module)
            elif module == 'question' and template:
                return self._get_question_message(module)
            elif module == 'announcement' and template:
                return self._get_announcement_message(module)

    def get_summary(self, value):
        summary_size = 50
        if len(value) > summary_size:
            return u'{}...'.format(value[:summary_size])

        else:
            return value
