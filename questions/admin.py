from django.contrib import admin

from questions.models import Answer, Question


class AnswerAdminInline(admin.TabularInline):
    model = Answer


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'create_date')
    search_fields = ('user', 'title', 'description')
    inlines = (AnswerAdminInline, )


admin.site.register(Question, QuestionAdmin)
