# coding: utf-8

from django.conf.urls import url

from questions import views

urlpatterns = [
    url(r'^$', views.QuestionsView.as_view(), name='questions'),
    url(r'^answered/$', views.AnsweredQuestionView.as_view(), name='answered'),
    url(r'^unanswered/$', views.UnansweredQuestionView.as_view(), name='unanswered'),
    url(r'^all/$', views.AllQuestionsView.as_view(), name='all'),
    url(r'^ask/$', views.AskQuestionView.as_view(), name='ask'),
    url(r'^favorite/$', views.FavoriteQuestionView.as_view(), name='favorite'),
    url(r'^answer/$', views.AnswerQuestionView.as_view(), name='answer'),
    url(r'^answer/accept/$', views.AcceptQuestionView.as_view(), name='accept'),
    url(r'^answer/vote/$', views.VoteQuestionView.as_view(), name='vote'),
    url(r'^(\d+)/$', views.GetQuestionView.as_view(), name='question'),
]
