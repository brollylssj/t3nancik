from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import View

from activities.models import Activity
from questions.forms import QuestionForm, AnswerForm
from questions.models import Question, Answer
from tenancik.decorators import ajax_required


class QuestionsBaseView(TemplateView):
    template_name = 'questions/questions.html'

    def get_paginated_questions(self, request, questions, active):
        paginator = Paginator(questions, 10)
        page = request.GET.get('page')
        try:
            questions = paginator.page(page)
        except PageNotAnInteger:
            questions = paginator.page(1)
        except EmptyPage:
            questions = paginator.page(paginator.num_pages)
        return render(request, self.template_name, {
            'questions': questions,
            'active': active
        })


class AnsweredQuestionView(QuestionsBaseView):
    @method_decorator(login_required)
    def get(self, request):
        questions = Question.get_answered()
        return self.get_paginated_questions(request, questions, 'answered')


class UnansweredQuestionView(QuestionsBaseView):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        questions = Question.get_unanswered()
        return self.get_paginated_questions(request, questions, 'unanswered')


class QuestionsView(QuestionsBaseView):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return UnansweredQuestionView.as_view()(request)


class AllQuestionsView(QuestionsBaseView):
    @method_decorator(login_required)
    def get(self, request):
        questions = Question.objects.all()
        return self.get_paginated_questions(request, questions, 'all')


class AskQuestionView(TemplateView):
    template_name = 'questions/ask.html'
    class_form = QuestionForm

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        form = self.class_form()
        return render(request, self.template_name, {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.class_form(request.POST)
        if form.is_valid():
            question = Question(user=request.user, title=form.cleaned_data.get('title'),
                                description=form.cleaned_data.get('description'))
            question.save()
            tags = form.cleaned_data.get('tags')
            question.create_tags(tags)
            return redirect('/questions/')
        else:
            return render(request, self.template_name, {'form': form})


class GetQuestionView(TemplateView):
    template_name = 'questions/question.html'

    @method_decorator(login_required)
    def get(self, request, pk):
        question = get_object_or_404(Question, pk=pk)
        form = AnswerForm(initial={'question': question})
        context = {
            'question': question,
            'form': form
        }
        return render(request, self.template_name, context)


class AnswerQuestionView(TemplateView):
    template_name = 'questions/question.html'
    class_form = AnswerForm

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return redirect('/questions/')

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.class_form(request.POST)
        if form.is_valid():
            user = request.user
            answer = Answer(user=user, question=form.cleaned_data.get('question'),
                            description=form.cleaned_data.get('description'))
            answer.save()
            user.profile.notify_answered(answer.question)
            return redirect(u'/questions/{}/'.format(answer.question.pk))
        else:
            question = form.cleaned_data.get('question')
            context = {
                'question': question,
                'form': form
            }
            return render(request, self.template_name, context)


class AcceptQuestionView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        answer_id = request.POST['answer']
        answer = Answer.objects.get(pk=answer_id)
        user = request.user
        user.profile.unotify_accepted(answer.question.get_accepted_answer())
        if answer.question.user == user:
            answer.accept()
            user.profile.notify_accepted(answer)
            return HttpResponse()
        return HttpResponseForbidden()


class VoteQuestionView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        answer_id = request.POST['answer']
        answer = Answer.objects.get(pk=answer_id)
        vote = request.POST['vote']
        user = request.user
        activity = Activity.objects.filter(Q(activity_type=Activity.UP_VOTE) | Q(activity_type=Activity.DOWN_VOTE),
                                           user=user, answer=answer_id)
        if activity:
            activity.delete()
        if vote in [Activity.UP_VOTE, Activity.DOWN_VOTE]:
            activity = Activity(activity_type=vote, user=user, answer=answer_id)
            activity.save()
        return HttpResponse(answer.calculate_votes())


class FavoriteQuestionView(View):
    @method_decorator(login_required)
    @method_decorator(ajax_required)
    def post(self, request, *args, **kwargs):
        question_id = request.POST['question']
        question = Question.objects.get(pk=question_id)
        user = request.user
        activity = Activity.objects.filter(activity_type=Activity.FAVORITE, user=user, question=question_id)
        if activity:
            activity.delete()
            user.profile.unotify_favorited(question)
        else:
            activity = Activity(activity_type=Activity.FAVORITE, user=user, question=question_id)
            activity.save()
            user.profile.notify_favorited(question)
        return HttpResponse(question.calculate_favorites())
