
class UploadFileException(Exception):
    pass


class SaveFileException(Exception):
    pass