import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView
from django.views.generic import View

from core.constants import USER_AMOUNT_ON_ONE_NETWORK_SITE
from core.exceptions import UploadFileException, SaveFileException
from core.forms import ProfileForm, ChangePasswordForm
from core.upload_file import UploadFileManager
from feeds.constants import AMOUNT_FEEDS_ON_PAGE
from feeds.models import Feed
from feeds.views import FeedsView

logger = logging.getLogger(__name__)


class HomeView(TemplateView):
    template_name = 'core/cover.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return FeedsView.as_view()(self.request)
        return render(request, self.template_name)


class NetworkView(TemplateView):
    template_name = 'core/network.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        users_list = User.objects.filter(is_active=True).order_by('username')
        paginator = Paginator(users_list, USER_AMOUNT_ON_ONE_NETWORK_SITE)
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)
        return render(request, 'core/network.html', {'users': users})


class ProfileView(TemplateView):
    template_name = 'core/profile.html'

    @method_decorator(login_required)
    def get(self, request, username):
        page_user = get_object_or_404(User, username=username)
        all_feeds = Feed.get_feeds().filter(user=page_user)
        paginator = Paginator(all_feeds, AMOUNT_FEEDS_ON_PAGE)
        feeds = paginator.page(1)
        from_feed = -1
        if feeds:
            from_feed = feeds[0].id
        context = {
            'page_user': page_user,
            'feeds': feeds,
            'from_feed': from_feed,
            'page': 1
        }
        return render(request, self.template_name, context)


class SettingsView(TemplateView):
    template_name = 'core/settings.html'
    class_form = ProfileForm

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        initial = {
            'location': user.profile.location,
            'birth_date': user.profile.birth_date,
        }
        form = self.class_form(instance=user, initial=initial)
        return render(request, self.template_name, {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = request.user
        form = self.class_form(request.POST)
        if form.is_valid():
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.email = form.cleaned_data.get('email')
            user.profile.location = form.cleaned_data.get('location')
            user.profile.birth_date = form.cleaned_data.get('birth_date')
            user.save()
            messages.add_message(request, messages.SUCCESS, 'Your profile was successfully edited.')
        return render(request, self.template_name, {'form': form})


class ChangePasswordView(TemplateView):
    template_name = 'core/change_password.html'
    class_form = ChangePasswordForm

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        form = self.class_form(instance=user)
        return render(request, self.template_name, {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = request.user
        form = self.class_form(request.POST)
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            user.set_password(new_password)
            user.save()
            messages.add_message(request, messages.SUCCESS, 'Your password was successfully changed.')
        return render(request, self.template_name, {'form': form})


class PictureChangeView(TemplateView):
    template_name = 'core/picture.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        uploaded_picture = False
        try:
            if request.GET.get('upload_picture') == 'uploaded':
                uploaded_picture = True
        except Exception as e:
            logger.error('Problem with change file: %s', e.message)
            messages.add_message(request, messages.WARNING, 'Some problem was occurred during picture change')
        return render(request, self.template_name, {'uploaded_picture': uploaded_picture})


class UploadPictureView(View):
    @method_decorator(cache_page(1))
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        upload_file_manager = UploadFileManager(request)
        try:
            upload_file_manager.select_picture()
        except UploadFileException:
            messages.add_message(request, messages.WARNING, 'Some problem was occurred during picture upload.')
            return redirect('/settings/picture/')
        return redirect('/settings/picture/?upload_picture=uploaded')


class SavePictureView(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        upload_file_manager = UploadFileManager(request)
        try:
            upload_file_manager.save_picture()
        except SaveFileException:
            messages.add_message(request, messages.WARNING, 'Some problem was occurred during picture save.')
        return redirect('/settings/picture/')
