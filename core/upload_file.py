import logging
import os

from PIL import Image
from django.conf import settings

from core.constants import MAX_WIDTH_UPLOAD_FILE
from core.exceptions import UploadFileException, SaveFileException

logger = logging.getLogger(__name__)


class UploadFileManager(object):
    def __init__(self, request):
        self.request = request

    def select_picture(self):
        try:
            profile_pictures_path = self._get_or_create_profile_path()
            file = self.request.FILES['picture']
            filename = profile_pictures_path + self._get_username() + '_tmp.jpg'
            if os.path.exists(filename):
                os.remove(filename)
            with open(filename, 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            image = Image.open(filename)
            width, height = image.size
            if width > MAX_WIDTH_UPLOAD_FILE:
                new_width = MAX_WIDTH_UPLOAD_FILE
                new_height = (height * MAX_WIDTH_UPLOAD_FILE) / width
                new_size = new_width, new_height
                image.thumbnail(new_size, Image.ANTIALIAS)
                image.save(filename)
        except Exception as e:
            logger.error('Some problem with upload file %s', e.message)
            raise UploadFileException

    def save_picture(self):
        try:
            h, w, x, y = self._get_dimensions()
            tmp_filename = self.get_user_profile_path() + '_tmp.jpg'
            filename = self.get_user_profile_path() + '.jpg'
            im = Image.open(tmp_filename)
            cropped_im = im.crop((x, y, w + x, h + y))
            cropped_im.thumbnail((200, 200), Image.ANTIALIAS)
            cropped_im.save(filename)
            os.remove(tmp_filename)
        except Exception as e:
            logger.error('Some problem with saving file %s', e.message)
            raise SaveFileException

    def get_user_profile_path(self):
        return settings.MEDIA_ROOT + '/profile_pictures/' + self._get_username()

    def _get_username(self):
        return self.request.user.username

    def _get_dimensions(self):
        x = int(self.request.POST.get('x'))
        y = int(self.request.POST.get('y'))
        w = int(self.request.POST.get('w'))
        h = int(self.request.POST.get('h'))
        return h, w, x, y

    def _get_or_create_profile_path(self):
        profile_pictures = settings.MEDIA_ROOT + '/profile_pictures/'
        if not os.path.exists(profile_pictures):
            os.makedirs(profile_pictures)
        return profile_pictures
